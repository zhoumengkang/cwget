#include <stdio.h>
#include <string.h>

char *parse_scheme(const char *url)
{
    char *p = strstr(url,"://");
    long l = p - url + 1;
    char scheme[l];
    memset(scheme,0,l);
    strncpy(scheme,url,l-1);
    return scheme;
}

int main()
{
    const char *url = "http://static.mengkang.net/abc.jpg";
    char *scheme = parse_scheme(url);
    printf("%s\n",scheme);
    return 0;
}


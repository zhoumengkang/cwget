#include <stdio.h>
#include <string.h>

char *parse_scheme(const char *url)
{
    char *p = strstr(url,"://");
    p[0] = '\0';
    return url; // http
}

int main()
{
    const char *url = "http://static.mengkang.net/abc.jpg";
    char *scheme = parse_scheme(url);
    printf("%s\n",scheme);
    return 0;
}

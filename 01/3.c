#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *parse_scheme(const char *url1)
{
    // 先拷贝
    int len = strlen(url1);
    char *url = (char *)malloc(len+1);
    strcpy(url,url1);
    // 再操作
    char *p = strstr(url,"://");
    p[0] = '\0';
    return url; // http
}

int main()
{
    const char *url = "http://static.mengkang.net/abc.jpg";
    char *scheme = parse_scheme(url);
    printf("%s\n",scheme);
    return 0;
}

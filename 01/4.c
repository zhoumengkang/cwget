#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *parse_scheme(const char *url)
{
    char *p = strstr(url,"://");
    if (p == NULL) {
        printf("url 非法\t%s\n",url);
        exit(1);
    }

    long l = p - url;
    char *scheme = (char *)malloc(l+1);
    memset(scheme,0,l+1);

    strncpy(scheme,url,l);

    return scheme;
}

int main()
{
    const char *url = "http://static.mengkang.net/abc.jpg";
    char *scheme = parse_scheme(url);
    printf("%s\n",scheme);
    return 0;
}

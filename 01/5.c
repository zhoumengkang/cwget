#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *parse_scheme(const char *url)
{
    char *p = strstr(url,"://");
    if (p == NULL) {
        printf("url 非法\t%s\n",url);
        exit(1);
    }

    char *scheme = strndup(url,p-url);

    return scheme;
}

int main()
{
    const char *url = "http://static.mengkang.net/abc.jpg";
    char *scheme = parse_scheme(url);
    printf("%s\n",scheme);
    return 0;
}

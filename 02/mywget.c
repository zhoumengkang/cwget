#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "url.h"

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("%s\n","请输入url");
        exit(1);
    }
    
    const char *url = argv[1];

    char * scheme = parse_scheme(url);
    char *host = parse_host(url,scheme);

    free(scheme);
    free(host);
    return 0;
}


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "url.h"

char *parse_scheme(const char *url)
{
    char *p = strstr(url,"://");

    if (p == NULL) {
        printf("非法url\n");
        exit(1);
    }

    char *scheme = strndup(url,p-url);

    printf("scheme:\t%s\n",scheme);

    return scheme;

}


/**
 * https://a.com/bbbb
 * https://a.com?b=/c
 * https://a.com#dddd
 * https://a.com
 */
char *parse_host(const char *url,const char *scheme)
{
    url = url + strlen(scheme) + strlen("://");

    char a[3] = {'/', '?', '#'};
    int p = 0;
    int min = 0;
    char *s = NULL;

    for(int i=0;i < 3;i++) {
        s = strchr(url,a[i]);
        if (s != NULL) {
            p = s - url;
            if (i == 0 || min > p) {
                min = p;
            }
        }
    }

    if (min == 0) {
        min = strlen(url);
    }

    char *host = strndup(url,min);

    printf("host:\t%s\n",host);

    return host;
}

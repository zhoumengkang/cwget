#ifndef MYWGT_URL_H
#define MYWGT_URL_H

char *parse_scheme(const char *url);
char *parse_host(const char *url,const char *scheme);

#endif

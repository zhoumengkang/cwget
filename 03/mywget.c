#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "url.h"

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("%s\n","请输入url");
        exit(1);
    }
    
    const char *url = argv[1];

    char * scheme = parse_scheme(url);
    char *host = parse_host(url,scheme);
    char *domain = parse_domain(host);
    int port = parse_port(host,scheme);
    char *ip = parse_ip(domain);
    char *uri = parse_uri(url,scheme,host);

    free(scheme);
    free(host);
    free(domain);
    free(uri);
    return 0;
}


#ifndef MYWGET_WGET_H
#define MYWGET_WGET_H

#include "url.h"

char *create_http_request(http_url *url);

int wget(http_url *url, const char *filename);

#endif

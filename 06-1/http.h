#ifndef MYWGET_HTTP_H
#define MYWGET_HTTP_H

#include "url.h"

typedef struct {
    unsigned int http_code;
    unsigned long content_length;
    char *content_type;
} http_response_header;

char *create_http_request(http_url *url);

http_response_header *http_response_header_prase(const char *http_response_header_str);

#endif

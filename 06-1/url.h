#ifndef MYWGT_URL_H
#define MYWGT_URL_H

typedef struct{
    char *scheme;
    char *domain;
    char *host;
    int port;
    char *ip;
    char *uri;
} http_url;


char *parse_scheme(const char *url);
char *parse_host(const char *url,const char *scheme);
char *parse_domain(const char *host);
int parse_port(const char *host, const char *scheme);
char *parse_ip(const char *domain);
char *parse_uri(const char *url, const char *scheme, const char *host);

#endif

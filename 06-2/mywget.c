#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "url.h"
#include "log.h"
#include "wget.h"

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("%s\n","请输入url");
        exit(1);
    }

    set_debug(true);
    
    const char *url = argv[1];

    char * scheme = parse_scheme(url);
    char *host = parse_host(url,scheme);
    char *domain = parse_domain(host);
    int port = parse_port(host,scheme);
    char *ip = parse_ip(domain);
    char *uri = parse_uri(url,scheme,host);

    http_url *wget_url = (http_url *)malloc(sizeof(http_url));

    wget_url->scheme = scheme;
    wget_url->host = host;
    wget_url->domain = domain;
    wget_url->port = port;
    wget_url->ip = ip;
    wget_url->uri = uri;

    wget(wget_url, "xxx.jpg");

    free(scheme);
    free(host);
    free(domain);
    free(uri);

    free(wget_url);

    return 0;
}


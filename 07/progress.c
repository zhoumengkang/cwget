#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>

int main()
{
    struct winsize size;
    ioctl(STDIN_FILENO, TIOCGWINSZ, &size);
    int cols = size.ws_col;

    // [%-97s]%d%%\r
    
    const char *progress = "[]100%";
    cols = cols - strlen(progress);

    char cols_str[10] = {0};
    sprintf(cols_str,"%d",cols);

    char progress_format[20] = {0};
    strcat(progress_format,"[%-");
    strcat(progress_format,cols_str);
    strcat(progress_format,"s]%d%%\r");

    char progress_bar[cols];
    memset(progress_bar,0,cols);

    for(int i=1;i<=cols;i++){
        strcat(progress_bar,"=");
        printf(progress_format,progress_bar,(i*100)/cols);
        fflush(stdout);
        usleep(10000);
    }

    return 0;
}


<?php
// https://mengkang.net/1412.html

$width = exec("tput cols");

$progress = "[]100%";
$width = $width - strlen($progress);

$progress = "[%-{$width}s]%d%%\r";

for($i=1;$i<=$width;$i++){
    printf($progress,str_repeat("=",$i),($i/$width)*100);
    usleep(30000);
}

echo "\n";

?>

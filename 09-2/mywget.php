<?php

class Demo
{
    private $programName = "mywget";
    private $argv;
    private $argc;

    public function __construct($argv, $argc)
    {
        $this->argv = $argv;
        $this->argc = $argc;
    }


    public function usage(int $status)
    {

        printf("Usage: %s [OPTION]... [URL]...\n", $this->programName);

        if ($status != 0) {
            fprintf(STDOUT, "Try '%s --help' for more information.\n", $this->programName);
        } else {
            fprintf(STDOUT, "  -O,  --output-document=FILE  write documents to FILE\n");
            fprintf(STDOUT, "  -d,  --debug                 output debug message\n");
            fprintf(STDOUT, "  -h,  --help                  display this help and exit\n");
            fprintf(STDOUT, "  -V,  --version               output version information and exit\n");
        }

        exit($status);
    }

    public function run()
    {
        $shortOpts = "O:Vv::dh";
        $longOpts = ["output-document:", "version", "verbose::", "debug", "help"];
        $options = getopt($shortOpts, $longOpts, $index);
        // var_export($options);

        if (isset($options["h"]) || isset($options["help"])) {
            $this->usage(0);
        }

        if (!isset($this->argv[$index])) {
            $this->usage(1);
        }

        $url = $this->argv[$index];

        echo $url;
    }
}

(new Demo($argv, $argc))->run();

// php demo.php -O a.jpg -d -Vh http://static.mengkang.net/upload/image/2019/1012/1570878677199291.png
// php bbb.php -O a.jpg -d http://static.mengkang.net/upload/image/2019/1012/1570878677199291.png
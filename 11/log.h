#ifndef MYWGET_LOG_H
#define MYWGET_LOG_H
#include <stdbool.h>

static bool mywget_is_debug = false;

#define debug_log(format, ...); if(is_debug()) { \
    printf("\033[32m%s:%05d:\t"format"\033[0m", __FILE__, __LINE__, ##__VA_ARGS__); \
}

#define error_log(format, ...); printf("\033[31m"format"\033[0m", ##__VA_ARGS__);

void set_debug(bool flag);

bool is_debug();

#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "url.h"
#include "log.h"
#include "wget.h"
#include "wget_opt.h"

int main(int argc, char *argv[])
{
    int optc;
    char *filename = NULL;

    while ((optc = getopt_long(argc, argv, "O:dVh", long_opts, NULL)) != -1) {
    //    printf("optc:%c\toptarg:%s\toptind:%d\n", optc, optarg, optind);

        switch (optc) {
            case 'O':
                filename = optarg;
                break;
            case 'd':
                set_debug(true);
                break;
            case 'V':
                printf("%s 0.0.1\n", PROGRAM_NAME);
                exit(0);
            case 'h':
                usage(0);
                break;
            case '?':
                usage(1);
                break;
        }
    }

    if (optind + 1 != argc) {
        usage(1);
    }

    const char *url = argv[optind];

    char * scheme = parse_scheme(url);
    char *host = parse_host(url,scheme);
    char *domain = parse_domain(host);
    int port = parse_port(host,scheme);
    char *ip = parse_ip(domain);
    char *uri = parse_uri(url,scheme,host);

    http_url *wget_url = (http_url *)malloc(sizeof(http_url));

    wget_url->scheme = scheme;
    wget_url->host = host;
    wget_url->domain = domain;
    wget_url->port = port;
    wget_url->ip = ip;
    wget_url->uri = uri;

    wget(wget_url, filename);

    free(scheme);
    free(host);
    free(domain);
    free(uri);

    free(wget_url);

    return 0;
}

